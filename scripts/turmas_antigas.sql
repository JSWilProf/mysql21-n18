SELECT min(t.dt_inicio) as '1ª Turma',
	   max(t.dt_inicio) as 'Última Turma',
       c.nome
from turmas t, cursos c
group by c.nome
order by '1ª Turma';