select nome
from professores
where id = (
	select professor_id
	from (
		SELECT count(*) as nr_aulas, professor_id
		FROM turmas
		group by professor_id
		order by nr_aulas desc
		limit 1
	) o_professor
);