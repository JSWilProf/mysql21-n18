select avg(qtd_turmas) 'média de turmas por mês'
from (
	select mid(dt_inicio, 1, 7) ano_mes, count(curso_id) qtd_turmas
	from turmas
	group by ano_mes
	order by ano_mes
) cursos_mes;

