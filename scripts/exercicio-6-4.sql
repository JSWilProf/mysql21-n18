select nome, email
from professores
where id not in (
	select professor_id
	from turmas
	where dt_inicio <= last_day(now()) + interval + 1 day and     -- inicio <= 1º dia do mês E
		  dt_fim    >  last_day(now()) + interval + 1 day  or     -- fim    >  1º dia do mês OU
		  dt_inicio >= last_day(now()) + interval + 1 day and     -- inicio >= 1º dia do mês E
		  dt_inicio <= last_day(last_day(now()) + interval 1 day) -- inicio <= Último dia do mês
)
order by nome;

