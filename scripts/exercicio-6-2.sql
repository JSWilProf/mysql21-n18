select c.nome, t.dt_inicio, t.dt_fim
from turmas t
	join cursos c
    on t.curso_id = c.id
where dt_inicio <= now()
  and dt_fim >= now()
order by c.nome, t.dt_inicio;