select count(distinct a.aluno_id) qtd_alunos
from turmas t
  inner join turmas_has_alunos a
  on t.id = a.turma_id
where t.dt_inicio <= now()
  and t.dt_fim >= now()
order by a.aluno_id