select max(qtd_alunos) as 'Nº Máximo', 
	   min(qtd_alunos) 'Nº Mínimo', 
       avg(qtd_alunos) 'Média'
from (
	SELECT count(*) as qtd_alunos
	FROM turmas_has_alunos as ta
	group by ta.turma_id
) lista_qtd_alunos;