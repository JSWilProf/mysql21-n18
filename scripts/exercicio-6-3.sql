select nome, email
from professores
where id not in (
	select professor_id
	from turmas
	where dt_inicio <= now()
	  and dt_fim >= now()
)