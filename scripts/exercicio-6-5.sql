select p.nome, c.nome, t.dt_inicio, t.dt_fim
from turmas t
  join professores p
  on t.professor_id = p.id
  join cursos c
  on t.curso_id = c.id
where t.dt_inicio <= last_day(now()) + interval + 1 day and      -- inicio <= 1º dia do mês E
	  t.dt_fim    >  last_day(now()) + interval + 1 day  or      -- fim    >  1º dia do mês OU
	  t.dt_inicio >= last_day(now()) + interval + 1 day and      -- inicio >= 1º dia do mês E
	  t.dt_inicio <= last_day(last_day(now()) + interval 1 day) -- inicio <= Último dia do mês
order by p.nome